import { expect } from "chai";
import "mocha";
import { add } from "./math";

describe("Add Function", () => {

  it("3 + 4 = 7", () => {
    expect(add(3, 4)).to.equal(7);
  });

});
