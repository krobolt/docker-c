FROM ubuntu:latest

RUN apt-get update && apt-get install build-essential manpages-dev mingw-w64 -y 

# 32bit : i686-w64-mingw32-gcc -o main32.exe main.c
# 64 bit : x86_64-w64-mingw32-gcc -o main64.exe main.c
#CMD ["gcc", "--version"]