CC      = gcc
CFLAGS  = -g

default: all
all: Hello
Hello: Hello.c
	$(CC) -Wall $(CFLAGS) -o Hello Hello.c